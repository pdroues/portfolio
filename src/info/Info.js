import self from "../img/self.png"
import nas from "../img/nas.jpg"
import serveurs from "../img/serveurs.jpg"
import appweb from "../img/appweb.png"
import homelab from "../img/homelab.jpg"
import api from "../img/API.webp"
import ballonsonde from "../img/ballonsonde.jpg"
import appsport from "../img/sport.jpg"

/* Hi there! Thanks for checking out my portfolio template. Be sure to read the comments to get a better understanding of
how to make this template work best for you! */

export let colors = ["rgb(0,255,164)", "rgb(166,104,255)"];
/*
I highly recommend using a gradient generator like https://gradientgenerator.paytonpierce.dev/ to generate a pair of colors that you like.
These colors will be used to style your name on the homepage, the background of your picture, and some other accents throughout
the site.
 */


/*
So let's get started! Some of the info below is pretty self-explanatory, like 'firstName' and 'bio'. I'll try to explain anything
that might not be obvious right off the bat :) I recommend looking at the template example live using "npm start" to get an idea
of what each of the values mean.
 */

export const info = {
    firstName: "Philémon",
    lastName: "Drouës",
    position: "Architecte SI en devenir",
    selfPortrait: self,
    gradient: `-webkit-linear-gradient(135deg, ${colors})`,
    baseColor: colors[0],
    miniBio: [
        {
            emoji: '🌎',
            text: 'basé à Toulouse'
        },
        {
            emoji: "💼",
            text: "Alternant au CNRS"
        },
        {
            emoji: "📚",
            text: "Étudiant en BUT3"
        },
        {
            emoji: "📧",
            text: "philemon.droues@gmail.com"
        }
    ],
    socials: [
        {
            link: "https://gitlab.com/pdroues",
            icon: "fa fa-gitlab",
            label: 'gitlab'
        },
        {
            link: "https://www.linkedin.com/in/philemon-droues/",
            icon: "fa fa-linkedin",
            label: 'linkedin'
        }
    ],
    bio: "Étudiant en BUT3 Informatique, je souhaite poursuivre mes études pour devenir Architecte SI. J'aime le réseau, l'automatisation, j'utilise surtout des applications open sources",
    langue: [
        {
            langue: 'français',
            level: ''
        },
        {
            langue: 'catalan',
            level: 'C1'
        },
        {
            langue: 'anglais',
            level: 'B1'
        },
        {
            langue: 'espagnol',
            level: 'B1'
        },
    ],
    programation:
        {
            proficientWith: ['git', 'ci', 'tdd','html', 'java', 'php', 'sql', 'arduio', 'scrum' ],
            exposedTo: ['css', 'python', 'c', 'no-sql']
        }
    ,
    administration:
        {
            proficientWith: ['cd', 'gitops', 'ldap', 'docker', 'proxmox', 'bash', 'réseau', 'linux'],
            exposedTo: ['slurm', 'esxi', 'podman', 'Kubernetes', 'infrastructure-as-code']
        },
    hobbies: [
        {
            label: 'FOOS',
            emoji: '🐧'
        },
        {
            label: 'manga',
            emoji: '📖'
        },
        {
            label: 'jeux-vidéo',
            emoji: '🖥️'
        }
    ],
    portfolio: [
        {
            title: "Concevoir et déployer un réseau d'entreprise",
            source: "https://github.com/paytonjewell", 
            image: serveurs,
            competences: [ 'Administrer', 'Collaborer' ]
        },
        {
            title: "Développer une application web",
            source: "https://gitlab.com/groups/ecojetons/-/archived",
            image: appweb,
            competences: [ 'Réaliser', 'Gérer', 'Conduire', 'Collaborer' ]
        },
        {
            title: "NAS",
            image: nas,
            competences: [ 'Administrer' ]
        },
        {
            title: "Homelab",
            image: homelab,
            competences: [ 'Administrer' ]
        },
        {
            title: "API REST",
            source: "https://gitlab.com/pdroues/api-rest",
            image: api,
            competences: [ 'Réaliser', 'Gérer', 'Collaborer' ]
        },
        {
            title: "Application de gestion d'un club de sport",
            source: "https://gitlab.com/pdroues/php-project",
            image: appsport,
            competences: [ 'Réaliser', 'Gérer', 'Collaborer' ]
        },
        {
            title: "Ballon Sonde",
            image: ballonsonde,
            competences: [ 'Réaliser', 'Collaborer' ]
        }
    ],
    competences: [
        {
            title: "Réaliser",
            color: "#b80905",
            description: [
                "Élaborer une application informatique",
                "Faire évoluer une application informatique",
                "Maintenir en conditions opérationnelles une application informatique"
            ],
            projets: [
                "Développer une application web",
                "API REST",
                "Application de gestion d'un club de sport",
                "Ballon Sonde"
            ]
        },
        {
            title: "Optimiser",
            color: "#ed8c51",
            description: [
                "Améliorer les performances des programmes dans des contextes contraints",
                "Limiter l’impact environnemental d’une application informatique",
                "Mettre en place des applications informatiques adaptées et efficaces"
            ],
            projets: []
        },
        {
            title: "Administrer",
            color: "#efba11",
            description: [
                "Déployer une nouvelle architecture technique",
                "Améliorer une infrastructure existante",
                "Sécuriser les applications et les services"
            ],
            projets: [
                "Concevoir et déployer un réseau d'entreprise",
                "NAS",
                "Homelab"
            ]
        },
        {
            title: "Gérer",
            color: "#8cc850",
            description: [
                "Lancer un nouveau projet",
                "Sécuriser des données",
                "Exploiter des données pour la prise de décisions"
            ],
            projets: [
                "Développer une application web",
                "API REST",
                "Application de gestion d'un club de sport"
            ]
        },
        {
            title: "Conduire",
            color: "#174AD4",
            description: [
                "Lancer un nouveau projet",
                "Piloter le maintien d’un projet en condition opérationnelle",
                "Faire évoluer un système d’information"
            ],
            projets: [
                "Développer une application web",
            ]
        },
        {
            title: "Collaborer",
            color: "#66757f",
            description: [
                "Lancer un nouveau projet",
                "Organiser son travail en relation avec celui de son équipe",
                "Élaborer, gérer et transmettre de l’information"
            ],
            projets: [
                "Développer une application web",
                "API REST",
                "Application de gestion d'un club de sport",
                "Ballon Sonde",
                "Concevoir et déployer un réseau d'entreprise",
            ]
        }
    ]
}