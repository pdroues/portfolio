import React from 'react';
import Style from './About.module.scss';
import Terminal from "./Terminal";
import {Box} from "@mui/material";
import {info} from "../../info/Info";


export default function About() {
    const firstName = info.firstName.toLowerCase()

    function aboutMeText() {
        return <>
            <p><span style={{color: info.baseColor}}>{firstName}{info.lastName.toLowerCase()} $</span> cat about-{firstName} </p>
            <p>{info.bio}</p>
        </>;
    }

    function langueText() {
        return <>
            <p><span style={{color: info.baseColor}}>{firstName}{info.lastName.toLowerCase()} $</span> systemctl status langue.service</p>
            <p><span style={{color: info.baseColor}}> <span className={Style.green}>active</span> <span className={Style.green}>enable</span></span></p>
            <ul>
                {info.langue.map((langue, index) => (
                    <li key={index}>{langue.langue}  {langue.level}</li>
                ))}
            </ul>
        </>;
    }

    function administrationText() {
        return <>
            <p><span style={{color: info.baseColor}}>skills <span className={Style.green}>(main)</span> $</span> less administration.md</p>
            <p><strong>### Éfficace avec</strong></p>
            <ul className={Style.skills}>
                {info.administration.proficientWith.map((proficiency, index) => <li key={index}>{proficiency}</li>)}
            </ul>
            <p><strong>### Notion en</strong></p>
            <ul className={Style.skills}>
                {info.administration.exposedTo.map((skill, index) => <li key={index}>{skill}</li>)}
            </ul>
        </>;
    }

    function programationText() {
        return <>
            <p><span style={{color: info.baseColor}}>{firstName}{info.lastName.toLowerCase()} $</span> cd skills/programation
            </p>
            <p><span style={{color: info.baseColor}}>skills/programation <span
                className={Style.green}>(main)</span> $</span> ls</p>
            <p><strong>Éfficace avec</strong></p>
            <ul className={Style.skills}>
                {info.programation.proficientWith.map((proficiency, index) => <li key={index}>{proficiency}</li>)}
            </ul>
            <p><strong>Notion en</strong></p>
            <ul className={Style.skills}>
                {info.programation.exposedTo.map((skill, index) => <li key={index}>{skill}</li>)}
            </ul>
        </>;
    }

    function miscText() {
        return <>
            <p><span style={{color: info.baseColor}}>{firstName}{info.lastName.toLowerCase()} $</span> cd
                hobbies/interests</p>
            <p><span style={{color: info.baseColor}}>hobbies/interests <span
                className={Style.green}>(main)</span> $</span> ls</p>
            <ul>
                {info.hobbies.map((hobby, index) => (
                    <li key={index}><Box component={'span'} mr={'1rem'}>{hobby.emoji}</Box>{hobby.label}</li>
                ))}
            </ul>
        </>;
    }

    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} mt={'3rem'} style={{ marginTop: "50px" }}>
            <Terminal text={aboutMeText()}/>
            <Terminal text={administrationText()}/>
            <Terminal text={programationText()}/>
            <Terminal text={langueText()}/>
            <Terminal text={miscText()}/>
        </Box>
    )
}