import React, { useState } from 'react';
import CompetencesBlock from "./CompetencesBlock";
import {Box, Grid} from "@mui/material";
import {info} from "../../info/Info";

export default function Competences() {

    const [hoveredIndex, setHoveredIndex] = useState(null);
  
    const handleHover = (index) => {
      setHoveredIndex(index);
    };

    return (
        <Box style={{ marginTop: "50px" }}>
            <Grid container display={'flex'} justifyContent={'center'}>
                {info.competences.map((competence, index) => (
                   <Grid item xs={12} md={6} key={index} onMouseEnter={() => handleHover(index)} onMouseLeave={() => handleHover(null)} style={{ padding: "30px", backgroundColor: competence.color, borderRadius: "10px", margin: "10px"}}>
                       <CompetencesBlock title={competence.title} description={competence.description} projets={competence.projets} isHovered={index === hoveredIndex}/>
                   </Grid>
                ))}
            </Grid>
        </Box>
    );
};