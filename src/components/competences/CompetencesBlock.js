import React from 'react';
import {Box} from "@mui/material";

function CompetencesBlock(props) {
   const {image, title, description, isHovered, projets} = props;
   return (
      <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'} >
         <h1 style={{fontSize: '2rem'}}>{title}</h1>
         <Box className={'competences'} display={'flex'} flexDirection={'column'} gap={'0.5rem'}
              alignItems={'left'} fontSize={'1.5rem'}>
               {!isHovered && (
               <div class="bordel">
                  <h2>Compétence</h2>
                  <ul>
                     {description.map((item, index) => (
                        <li key={index}>{item}</li>
                     ))}
                  </ul>
               </div>
               )}
               {isHovered && (
               <div class="bordel">
                  <h2>Projets</h2>
                  <ul>
                     {projets.map((item, index) => (
                        <a href="../portfolio"><li key={index}>{item}</li></a>
                     ))}
                  </ul>
               </div>
               )}
         </Box>
      </Box>
   );
}

export default CompetencesBlock;