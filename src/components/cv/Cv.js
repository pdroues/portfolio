import React from 'react';
import {Box} from "@mui/material";

export default function Cv() {

   return (
      <Box component={'main'} display={'flex'} flexDirection={{xs: 'column', md: 'row'}} alignItems={'center'}
           justifyContent={'center'} minHeight={'calc(100vh - 175px)'} style={{ marginTop: "50px" }}>
            <iframe title="CV" src={`${process.env.PUBLIC_URL}/cv.pdf`} width="700px" height="800px" frameBorder="0">
                This browser does not support PDFs. Please download the PDF to view it.
            </iframe>
        </Box>
   )
}